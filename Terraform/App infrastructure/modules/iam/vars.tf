variable aws_region {
  description = "This variable is used to specify the region for the resources"
  type        = string
  default     = ""
}

variable aws_credentials_file {
  description = "This variable points terraform at the aws credentials file"
  type        = string
  default     = ""
}

variable aws_profile {
  description = "This variable point terraform at the aws credentials file"
  type        = string
  default     = ""
}

variable project {
  description = "The project name"
  type        = string
  default     = "ibos"
}

variable environment {
  description = "This specifies the environment which can be either dev or test"
  type        = string
  default     = "def"
}

# IAM
variable ec2_ssm_role_name {
  description = "Name for the role that will be attached to the EC2 for SSM access "
  type        = string
  default     = "ec2-ssm-role"
}

variable ssm_instance_profile_name {
  description = "This the name of the instance profile name"
  type        = string
  default     = "ssm-instance-profile"
}

variable ssm_policy_name {
  description = "This the name of the ssm policy which grants SSM permissions"
  type        = string
  default     = "ssm-policy"
}



# About
This Terraform module can be used to deploy IAM resources.
# Resources
1. IAM role
2. IAM instance profile
3. IAM policy
4. Policy attachment


# Variables
The following variables are required for this configuration to successfully deploy all resources:

| Variable name | Description |
|---------------|-------------|
| aws_region | This variable is used to specify the region for the resources |
| aws_credentials_file | This is the file containing AWS credentials |
| aws_profile | The name of the AWS profile to use |
| project | The project name, added as a tag to resoruces (defaults to ibos) |
| environment | The environment, used to tag resources (defaults to dev) |
| ec2_ssm_role_name | Name for the role that will be attached to the EC2 for SSM access |
| ssm_instance_profile_name | This the name of the instance profile name |
| ssm_policy_name | This the name of the ssm policy which grants SSM permissions |


# Outputs

| Output name | Description |
|-------------|-------------|
| instance-profile-name | The name of the instance profile |

# Deployment
## Prerequisites
1. Terraform installed
2. AWS IAM user and access to the Inov8 account and AWS CLI installed and configured
3. Variables specified (use terraform.tfvars file):
eg.
```terraform
aws_region           = "eu-west-2"
aws_credentials_file = "C:\\Users\\USER_NAME\\.aws\\credentials"
aws_profile          = "PROFILE_NAME"
project              = "PROJECT_NAME"
environment          = "ENVIRONMENT_NAME"

ec2_ssm_role_name         = "test-role"
ssm_instance_profile_name = "test-instance-profile"
ssm_policy_name           = "test-policy"
```
4.terraform.tf file with the backend and provider configured
eg.
```terraform
provider "aws" {
  version                 = "~>2.0"
  region                  = var.aws_region
  shared_credentials_file = var.aws_credentials_file
  profile                 = var.aws_profile
}

terraform {
  backend "s3" {
    encrypt = "true"
    bucket  = "BUCKET_NAME"
    key     = "FOLDER/terraform.tfstate"
    region  = "REGION"
    profile = "PROFILE_NAME"
  }
}
```

## Deployment steps
1. Clone the repo
2. Redirect to the REPO\Terraform\App infrastructure\Terraform\modules\vpc
3. Run `terraform init` to initialize backend (if executing for the first time) and download plugins and modules
4. Run `terraform plan -out PLAN_NAME` to create a deployment plan and what resources would be deployed. If using .tfvars with a name other than terraform.tfvars, you will have to specify -var-file=TFVARS_FILE_NAME attribute. eg. `terraform plan -out my_plan -var-file=test.tfvars`
5. Run `terraform apply PLAN_NAME` to deploy infrastructure using the plan created in the previous step

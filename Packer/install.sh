#!/bin/sh -x
# Install Python and flask AMAZON-Linux
sudo yum install python36 -y
sudo pip install --upgrade pip
mkdir my_flask_app
cd my_flask_app
python3 -m venv venv
source venv/bin/activate
pip install Flask


#Install Java for Jenkins Slave
sudo yum -y update
sudo yum -y install java-1.8.0-openjdk

#Install Docker
# yum install epel-release (it is already installed on the reference AMI)
sudo yum -y install docker-io
sudo service docker start
sudo service docker status
sudo chkconfig docker on


# Install Python and flask UBUNTU

#sudo apt-get -y update
#sudo apt-get -y install python3-dev python3-pip
#mkdir my_flask_app
#cd my_flask_app
#sudo python3 -m venv venv
#source venv/bin/activate
#pip install Flask
#python -m flask --version


-- -------------------------------------------------------------------------------- 
-- 
-- ibos_hr_employee_name.sql
-- 
-- Build table for employee names to integrate with iBOS CRM system 
-- 
-- Created Objects:
-- ibos_hr.employee_name, 
--
-- Used Objects: 
-- 
-- 
-- Ver      Date            Author          Comment
-- 1.00     2020-03-19      Chris Black     JIRA reference: IBOS-6   Initial version
-- 
-- -------------------------------------------------------------------------------
USE ibos_hr;

-- DROP TABLE IF EXISTS ibos_hr.employee_name;

-- Define variable names, formats, keys
CREATE TABLE ibos_hr.employee_name (
  employee_id          INT            COMMENT 'Primary key'                                PRIMARY KEY AUTO_INCREMENT 
  ,user_id             VARCHAR(10)    COMMENT 'Employee user identifier'                   NOT NULL  
  ,title               VARCHAR(10)    COMMENT 'Title'                                      NOT NULL
  ,first_name          VARCHAR(50)    COMMENT 'Employee first name'                        NOT NULL
  ,last_name           VARCHAR(100)   COMMENT 'Employee last name'                         NOT NULL   	
  ,other_names         VARCHAR(200)   COMMENT 'Middle names'    
  ,date_of_birth       DATE           COMMENT 'Employee date of birth'                     NOT NULL
  ,ni_number           VARCHAR(20)    COMMENT 'National Insurance number'                  NOT NULL
)
COMMENT 'Contains employee names, date of birth and national insurance number';

-- Insert sample data values into table
INSERT INTO ibos_hr.employee_name (
  user_id  
  ,title              
  ,first_name          
  ,last_name
  ,other_names       
  ,date_of_birth
  ,ni_number
) 

VALUES 
 (
 -- user_id  
'BLACKC1'
--   ,title        
, 'Mr' 
--   ,first_name    
, 'Chris'
--   ,last_name
, 'Black'
--   ,other_names    
, 'James'
--   ,date_of_birth
, DATE '1979-04-27'
--   ,ni_number
, 'JS9876543B'
),
 (
 -- user_id  
'DUNCANS1'
--   ,title        
, 'Mr' 
--   ,first_name    
, 'Steven'
--   ,last_name
, 'Duncan'
--   ,other_names    
, ''
--   ,date_of_birth
, DATE '1990-01-01'
--   ,ni_number
, 'XY123456X'
),
(
 -- user_id  
'MATHERC1'
--   ,title        
, 'Mr' 
--   ,first_name    
, 'Calum'
--   ,last_name
, 'Mather'
--   ,other_names    
, ''
--   ,date_of_birth
, DATE '1980-01-01'
--   ,ni_number
, 'AB904578D'
)
;





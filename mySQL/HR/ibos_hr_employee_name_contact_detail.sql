-- -------------------------------------------------------------------------------- 
-- 
-- ibos_hr_employee_name_contact_details.sql
-- 
-- Build mapping table for employee name to employee contact details integrate with iBOS CRM system 
-- 
-- Created Objects:
-- ibos_hr.employee_name_contact_details
--
-- Used Objects:
-- 
-- 
-- Ver      Date            Author          Comment
-- 1.00     2020-04-03      Chris Black     JIRA reference: IBOS-6   Initial version
-- 
-- -------------------------------------------------------------------------------
USE ibos_hr;

-- DROP TABLE IF EXISTS ibos_hr.employee_name_contact_details;

-- create mapping table role to contact

CREATE TABLE ibos_hr.employee_name_contact_details (
  id                 INT          COMMENT 'record id'                         PRIMARY KEY  AUTO_INCREMENT 
  ,employee_id       INT          COMMENT 'employee ID'                       ,FOREIGN KEY (employee_id) REFERENCES employee_name (employee_id)   
  ,contact_id        INT          COMMENT 'contact ID'                        ,FOREIGN KEY (contact_id) REFERENCES employee_contact_details (contact_id)   
  ,contact_start     DATETIME     COMMENT 'Start date for contact'            NOT NULL 
  ,contact_end       DATETIME     COMMENT 'End date for contact'              NOT NULL
)
COMMENT 'Maps employee id to contact id';

INSERT INTO ibos_hr.employee_name_contact_details
SELECT null AS id
	   ,en.employee_id
     ,ea.contact_id
     ,DATE '2020-01-01' AS contact_start
     ,DATE '9999-12-31' AS contact_end
FROM ibos_hr.employee_name en
LEFT JOIN ibos_hr.employee_contact_details ea ON en.user_id = ea.user_id;

UPDATE ibos_hr.employee_name_contact_details
SET contact_start = DATE ('2020-03-01')
WHERE contact_id IN (4);

UPDATE ibos_hr.employee_name_contact_details
SET contact_end = DATE ('2020-02-29')
WHERE contact_id IN (3);

SELECT * FROM ibos_hr.employee_name_contact_details;




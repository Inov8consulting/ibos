-- -------------------------------------------------------------------------------- 
-- 
-- ibos_hr_employee_name_address.sql
-- 
-- Build table for mapping employee name to employee address to integrate with iBOS CRM system 
-- 
-- Created Objects:
-- ibos_hr.employee_name_address
--
-- Used Objects:
-- 
-- 
-- Ver      Date            Author          Comment
-- 1.00     2020-04-03      Chris Black     JIRA reference: IBOS-6     Initial version
-- 
-- -------------------------------------------------------------------------------
USE ibos_hr;

-- DROP TABLE IF EXISTS ibos_hr.employee_name_address;

-- create mapping table role to address

CREATE TABLE ibos_hr.employee_name_address (
  id                 INT          COMMENT 'record id'                         PRIMARY KEY  AUTO_INCREMENT 
  ,employee_id       INT          COMMENT 'employee ID'                       ,FOREIGN KEY (employee_id) REFERENCES employee_name (employee_id)   
  ,address_id        INT          COMMENT 'address ID'                        ,FOREIGN KEY (address_id) REFERENCES employee_address (address_id)   
  ,address_start     DATETIME     COMMENT 'Start date for address'            NOT NULL 
  ,address_end       DATETIME     COMMENT 'End date for address'              NOT NULL
)
COMMENT 'Maps employee id to address id';

INSERT INTO ibos_hr.employee_name_address
SELECT null AS id
	   ,en.employee_id
     ,ea.address_id
     ,DATE '2020-01-01' AS address_start
     ,DATE '9999-12-31' AS address_end
FROM ibos_hr.employee_name en
LEFT JOIN ibos_hr.employee_address ea ON en.user_id = ea.user_id;

UPDATE ibos_hr.employee_name_address
SET address_start = DATE ('2020-03-01')
WHERE address_id IN (4);

UPDATE ibos_hr.employee_name_address
SET address_end = DATE ('2020-02-29')
WHERE address_id IN (3);




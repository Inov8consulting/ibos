-- -------------------------------------------------------------------------------- 
-- 
-- ibos_hr_employee_contract.sql
-- 
-- Build table for employee contract to integrate with iBOS CRM system 
-- 
-- Created Objects:
-- ibos_hr.employee_contract
--
-- Used Objects: 
-- 
-- 
-- Ver      Date            Author          Comment
-- 1.00     2020-03-19      Chris Black     IBOS-6    Initial version
-- 
-- -------------------------------------------------------------------------------
USE ibos_hr;

-- DROP TABLE IF EXISTS ibos_hr.employee_contract_1;

-- Define variable names, formats, keys
CREATE TABLE ibos_hr.employee_contract_1 (
   contract_id             INT            COMMENT 'contract ID'                                PRIMARY KEY AUTO_INCREMENT
  ,employee_id             INT            COMMENT 'Employee ID'                                NOT NULL 
  ,contract_date           DATETIME       COMMENT 'Contract date'
  ,start_date              DATETIME       COMMENT 'employee start date'
  ,continuous_start_date   DATETIME       COMMENT 'continuous start date'
 )
COMMENT 'Contains employee contract information';


-- Insert sample data values into table
INSERT INTO ibos_hr.employee_contract_1 (
  employee_id       
  ,contract_date          
  ,start_date              
  ,continuous_start_date
) 

VALUES (
--   employee_id
1
--   ,contract_date
, null
--   ,start_date
,DATE '2020-03-02'              
--   ,continuous_start_date
,DATE '2020-03-02'        
), 
(
--   employee_id
2
--   ,contract_date
,DATE '2019-02-20'
--   ,start_date
,DATE '2019-03-30'              
--   ,continuous_start_date
,DATE '2020-03-30'
),
(
--   employee_id
3
--   ,contract_date
,DATE '2000-06-05'
--   ,start_date
,DATE '2017-07-05'              
--   ,continuous_start_date
,DATE '2017-07-05'
)
;

-- DROP TABLE IF EXISTS ibos_hr.employee_contract;

-- Define variable names, formats, keys
CREATE TABLE ibos_hr.employee_contract (
   contract_id             INT            COMMENT 'contract ID'                                PRIMARY KEY AUTO_INCREMENT
  ,employee_id             INT            COMMENT 'Employee ID'                                NOT NULL ,FOREIGN KEY (employee_id) REFERENCES employee_name (employee_id)
  ,contract_date           DATETIME       COMMENT 'Contract date'
  ,start_date              DATETIME       COMMENT 'employee start date'
  ,continuous_start_date   DATETIME       COMMENT 'continuos start date'
  ,notice_period           VARCHAR(20)    COMMENT 'notice period'
  ,contract_issued         CHAR(1)        COMMENT 'contract issued'
  )
COMMENT 'Contains employee contract information';

-- contract table with calc for notice period

INSERT INTO ibos_hr.employee_contract (
SELECT contract_id
	 ,employee_id
     ,contract_date
     ,start_date
     ,continuous_start_date
     ,CASE 
          WHEN contract_date IS NULL THEN NULL
          WHEN (TIMESTAMPDIFF(MONTH, contract_date, CURDATE())) < 7
               THEN '1 week'
          WHEN (Date_format(From_Days(To_Days(Curdate()) - To_Days(contract_date)), '%Y')) + 4 > 12
               THEN '12 weeks'
          ELSE CONCAT_WS(' ', CAST((Date_format(From_Days(To_Days(Curdate()) - To_Days(contract_date)), '%Y')) + 4 AS CHAR), 'weeks')
	  END AS notice_period
     ,CASE WHEN contract_date IS NOT NULL THEN 'Y' ELSE 'N' END AS contract_issued
FROM ibos_hr.employee_contract_1
);
-- -------------------------------------------------------------------------------- 
-- 
-- ibos_hr_employee_address.sql
-- 
-- Build table for employee address to integrate with iBOS CRM system 
-- 
-- Created Objects:
-- ibos_hr.employee_address, ibos_hr.employee_address_last
--
-- Used Objects: 
-- 
-- 
-- Ver      Date            Author          Comment
-- 1.00     2020-03-19      Chris Black     JIRA reference: IBOS-6     Initial version
-- 
-- -------------------------------------------------------------------------------
USE ibos_hr;

-- DROP TABLE IF EXISTS ibos_hr.employee_address;

-- Define variable names, formats, keys
CREATE TABLE ibos_hr.employee_address (
   address_id          INT            COMMENT 'Address ID'                        PRIMARY KEY AUTO_INCREMENT
  ,user_id             VARCHAR(10)    COMMENT 'Employee ID'                       NOT NULL 
  ,address_type        VARCHAR(20)    COMMENT 'Type of address i.e. home, office' NOT NULL 
  ,address_1           VARCHAR(200)   COMMENT 'First line of address'             NOT NULL 
  ,address_2           VARCHAR(200)   COMMENT 'Second line of address'             
  ,address_3           VARCHAR(200)   COMMENT 'Third line of address'              
  ,address_4           VARCHAR(200)   COMMENT 'Fourth line of address'             
  ,address_5           VARCHAR(200)   COMMENT 'Fifth line of address'              
  ,postcode            VARCHAR(20)    COMMENT 'Postal code'                       NOT NULL 
  
 )
COMMENT 'Contains employee address information';

-- Insert sample data values into table
INSERT INTO ibos_hr.employee_address (
   user_id        
  ,address_type        
  ,address_1           
  ,address_2                   
  ,address_3                       
  ,address_4                       
  ,address_5                        
  ,postcode            
 ) 

VALUES 
 (   
--   ,employee_id 
'BLACKC1'
--   ,address_type   
,'Home'     
--   ,address_1     
,'15 Broomburn Grove'         
--   ,address_2     
,''                
--   ,address_3     
,''                     
--   ,address_4     
,''                     
--   ,address_5    
,'Edinburgh'                       
--   ,postcode     
,'EH12 7NN'          
  
),
(   
--   ,employee_id 
'DUNCANS1'
--   ,address_type   
,'Home'     
--   ,address_1     
,'1 High Street'         
--   ,address_2     
,''                
--   ,address_3     
,''                     
--   ,address_4     
,''                     
--   ,address_5    
,'Edinburgh'                       
--   ,postcode     
,'EH1 1AB'          
--   ,address_start   
 
),
(   
--   ,employee_id 
'MATHERC1'
--   ,address_type   
,'Home'     
--   ,address_1     
,'1 Scott Street'         
--   ,address_2     
,''                
--   ,address_3     
,''                     
--   ,address_4     
,''                     
--   ,address_5    
,'Paisley'                       
--   ,postcode     
,'PA1 1AB'          
 
),
(   
--   ,employee_id 
'MATHERC1'
--   ,address_type   
,'Home'     
--   ,address_1     
,'1 Church Street'         
--   ,address_2     
,'Happytown'                
--   ,address_3     
,''                     
--   ,address_4     
,''                     
--   ,address_5    
,'Paisley'                       
--   ,postcode     
,'PA2 2CD'          
--   ,address_start   

);
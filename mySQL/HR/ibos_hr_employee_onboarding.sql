-- -------------------------------------------------------------------------------- 
-- 
-- ibos_hr_employee_onboarding.sql
-- 
-- Build table for employee onboarding to integrate with iBOS CRM system 
-- 
-- Created Objects:
-- ibos_hr.employee_onboarding
--
-- Used Objects: 
-- 
-- 
-- Ver      Date            Author          Comment
-- 1.00     2020-03-19      Chris Black     JIRA reference: IBOS-6   Initial version
--
-- -------------------------------------------------------------------------------
USE ibos_hr;

-- DROP TABLE IF EXISTS ibos_hr.employee_onboarding;

-- Define variable names, formats, keys
CREATE TABLE ibos_hr.employee_onboarding (
   onboarding_id               INT            COMMENT 'onboarding ID'               PRIMARY KEY AUTO_INCREMENT
  ,employee_id                 INT            COMMENT 'Employee ID'                 NOT NULL, FOREIGN KEY (employee_id) REFERENCES employee_name(employee_id)
  ,offer_acceptance            DATETIME       COMMENT 'date offer accepted'
  ,verifile_start              DATETIME       COMMENT 'date verifile check started' 
  ,bank_details_confirmed      DATETIME       COMMENT 'date bank details confirmed'
  ,bupa_form_sent              DATETIME       COMMENT 'date bupa form sent'
  ,it_requistion_sent          DATETIME       COMMENT 'date IT requistion'
  ,accountants_contacted       DATETIME       COMMENT 'date accountants contacted'
  ,personnel_folder_created    DATETIME       COMMENT 'date personnel folder created'
  )
COMMENT 'Contains employee onboarding information';

INSERT INTO ibos_hr.employee_onboarding(
 employee_id                 
  ,offer_acceptance           
  ,verifile_start            
  ,bank_details_confirmed  
  ,bupa_form_sent  
  ,it_requistion_sent   
  ,accountants_contacted      
  ,personnel_folder_created  
)
VALUES
(
-- employee_id            
 1    
--   ,offer_acceptance   
, DATE('2020-02-01')
--   ,verifile_start    
, DATE('2020-02-15')        
--   ,bank_details_confirmed  
, DATE('2020-02-15')
--   ,bupa_form_sent  
, DATE('2020-02-29')
--   ,it_requistion_sent   
, DATE('2020-02-29')
--   ,accountants_contacted    
, DATE('2020-03-01')  
--   ,personnel_folder_created 
, DATE('2020-03-01')
), 
(
-- employee_id            
 2    
--   ,offer_acceptance   
, DATE('2019-06-01')
--   ,verifile_start    
, DATE('2019-06-15')        
--   ,bank_details_confirmed  
, DATE('2019-06-17')
--   ,bupa_form_sent  
, DATE('2020-07-29')
--   ,it_requistion_sent   
, DATE('2020-07-29')
--   ,accountants_contacted    
, DATE('2020-07-01')  
--   ,personnel_folder_created 
, DATE('2020-06-30')
), 
(
-- employee_id            
 3    
--   ,offer_acceptance   
, DATE('2018-10-01')
--   ,verifile_start    
, DATE('2018-10-05')        
--   ,bank_details_confirmed  
, DATE('2018-11-15')
--   ,bupa_form_sent  
, DATE('2018-11-16')
--   ,it_requistion_sent   
, DATE('2018-10-15')
--   ,accountants_contacted    
, DATE('2019-01-01')  
--   ,personnel_folder_created 
, DATE('2019-01-02')
);


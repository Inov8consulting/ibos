-- -------------------------------------------------------------------------------- 
-- 
-- ibos_hr_employee_contact_details.sql
-- 
-- Build table for employee contact details to integrate with iBOS CRM system 
-- 
-- Created Objects:
-- ibos_hr.employee_contact_details, 
--
-- Used Objects: 
-- 
-- 
-- Ver      Date            Author          Comment
-- 1.00     2020-03-19      Chris Black     JIRA reference: IBOS-6 Initial version
-- 
-- -------------------------------------------------------------------------------
USE ibos_hr;

-- DROP TABLE IF EXISTS ibos_hr.employee_contact_details;

-- Define variable names, formats, keys
CREATE TABLE ibos_hr.employee_contact_details (
   contact_id          INT            COMMENT 'contact ID'                                 PRIMARY KEY AUTO_INCREMENT
  ,user_id             VARCHAR(10)    COMMENT 'Employee ID'                                NOT NULL 
  ,home_tel_num        VARCHAR(20)    COMMENT 'home telephone'
  ,mobile_tel_num      VARCHAR(20)    COMMENT 'mobile telephone'
  ,work_tel_num        VARCHAR(20)    COMMENT 'work telephone'
  ,personal_email_addr VARCHAR(200)   COMMENT 'Personal email address'                              
  ,work_email_addr     VARCHAR(200)   COMMENT 'INOV8 Email address'                              
  )
COMMENT 'Contains employee contact telephone and email addresses';

-- DELETE FROM ibos_hr.employee_contact;

-- Insert sample data values into table
INSERT INTO ibos_hr.employee_contact_details (
   user_id  
  ,home_tel_num
  ,mobile_tel_num
  ,work_tel_num
  ,personal_email_addr 
  ,work_email_addr
) 

VALUES 
 (   
--  user_id 
'BLACKC1'  
--   ,home_tel_num
, ''   
--   ,mobile_tel_num
, '07890012419' 
--   ,work_tel_num
, '01411234567'  
--   ,personal_email_addr 
, 'chrisblack7966@gmail.com'  
--   ,work_email_addr 
, 'christopher.black@inov8consulting.co.uk'         
  
), 
(--  employee_id 
'DUNCANS1'  
--   ,home_tel_num
, '01311234567'   
--   ,mobile_tel_num
, '07712345678' 
--   ,work_tel_num
, '01411234567'  
--   ,personal_email_addr 
, 'sduncan@gmail.com'  
--   ,work_email_addr 
, 'steven.duncan@inov8consulting.co.uk'     
), 
(   
--  employee_id 
'MATHERC1'  
--   ,home_tel_num
, '01417894561'   
--   ,mobile_tel_num
, '07897654123' 
--   ,work_tel_num
, '01411234567'  
--   ,personal_email_addr 
, 'cmather1@gmail.com'  
--   ,work_email_addr 
, 'calum.mather@inov8consulting.co.uk'    
), 
(--  employee_id 
'MATHERC1'  
--   ,home_tel_num
, '01319876543'   
--   ,mobile_tel_num
, '07897654123' 
--   ,work_tel_num
, '01411234567'  
--   ,personal_email_addr 
, 'cmather1@gmail.com'  
--   ,work_email_addr 
, 'calum.mather@inov8consulting.co.uk'     
)
;


-- -------------------------------------------------------------------------------- 
-- 
-- ibos_hr_employee_record.sql
-- 
-- Build table for employee record information 
-- 
-- Created Objects:
-- ibos_hr.employee_record
--
-- Used Objects: ibos_hr.employee_name, ibos_hr.employee_name_address, ibos_hr.employee_contact_details, ibos_hr.employee_contract, ibos_hr.employee_leaver
-- 
-- 
-- Ver      Date            Author          Comment
-- 1.00     2020-04-20      Chris Black     JIRA reference: IBOS-83 - Initial version
-- 
-- -----------------------------------------------------------------------------
USE ibos_hr;

DROP TABLE IF EXISTS ibos_hr.employee_record;
-- Define variable names, formats, keys
CREATE TABLE ibos_hr.employee_record (
  record_id            INT            COMMENT 'records id'                                 PRIMARY KEY AUTO_INCREMENT
  ,employee_id         INT            COMMENT 'employee ID'                                NOT NULL, FOREIGN KEY(employee_id) REFERENCES ibos_hr.employee_name(employee_id)
  ,user_id             VARCHAR(10)    COMMENT 'user ID'                                    NOT NULL  
  ,title               VARCHAR(10)    COMMENT 'Title'                                      NOT NULL
  ,first_name          VARCHAR(50)    COMMENT 'Employee first name'                        NOT NULL
  ,last_name           VARCHAR(100)   COMMENT 'Employee last name'                         NOT NULL   	
  ,other_names         VARCHAR(200)   COMMENT 'Middle names'    
  ,date_of_birth       DATE           COMMENT 'Employee date of birth'                     NOT NULL
  ,ni_number           VARCHAR(20)    COMMENT 'National Insurance number'                  NOT NULL
  ,current_address     VARCHAR(1020)  COMMENT 'Current address using end date'             NOT NULL
  ,home_tel_num        VARCHAR(20)    COMMENT 'Home telephone'                  
  ,mobile_tel_num      VARCHAR(20)    COMMENT 'Mobile telephone'                  
  ,personal_email_addr VARCHAR(200)   COMMENT 'employees personal email address'                  
  ,start_date          DATETIME       COMMENT 'Employees start date'                  
  ,leave_date          DATETIME       COMMENT 'Employees leave date'                  
  ,reason_for_leaving  VARCHAR(1000)  COMMENT 'Reason for leaving company'                  
)
COMMENT 'Contains employee name, address and contact details';

-- create a view of the employee record

INSERT INTO ibos_hr.employee_record 
SELECT NULL AS record_id
     ,en.employee_id
     ,en.user_id
     ,en.title
     ,en.first_name
     ,en.last_name
     ,en.other_names
     ,en.date_of_birth
     ,en.ni_number
     ,ea.current_address
     ,et.home_tel_num
     ,et.mobile_tel_num
     ,et.personal_email_addr
     ,ec.start_date
     ,el.leave_date
     ,el.reason_for_leaving
FROM ibos_hr.employee_name en
INNER JOIN (
     SELECT b.employee_id
          ,CONCAT_WS(' ', a.address_1, a.address_2, a.address_3, a.address_4, a.address_5, a.postcode) AS current_address
     FROM ibos_hr.employee_address a
     INNER JOIN ibos_hr.employee_name_address b ON a.address_id = b.address_id
     WHERE a.address_type = 'Home'
          AND b.address_end = DATE ('9999-12-31')
     ) ea ON en.employee_id = ea.employee_id
INNER JOIN (
     SELECT b.employee_id
          ,a.home_tel_num
          ,a.mobile_tel_num
          ,a.personal_email_addr
     FROM ibos_hr.employee_contact_details a
     INNER JOIN ibos_hr.employee_name_contact_details b ON a.contact_id = b.contact_id
     WHERE b.contact_end = DATE ('9999-12-31')
     ) et ON en.employee_id = et.employee_id
LEFT JOIN (
     SELECT ec1.contract_id
          ,ec1.employee_id
          ,ec1.start_date
          ,ec1.contract_date
     FROM ibos_hr.employee_contract ec1
     INNER JOIN (
          SELECT employee_id
               ,MAX(contract_date) AS latest_contract
          FROM ibos_hr.employee_contract
          GROUP BY employee_id
          ) ec2 ON ec1.employee_id = ec2.employee_id
          AND coalesce(ec1.contract_date, 0) = coalesce(ec2.latest_contract, 0)
     ) AS ec ON en.employee_id = ec.employee_id
LEFT JOIN ibos_hr.employee_leaver el ON en.employee_id = el.employee_id;
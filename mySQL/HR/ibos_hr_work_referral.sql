-- -------------------------------------------------------------------------------- 
-- 
-- ibos_hr_work_referral.sql
-- 
-- Build table for work referrals to integrate with iBOS CRM system 
-- 
-- Created Objects:
-- ibos_hr.work_referral
--
-- Used Objects: 
-- 
-- 
-- Ver      Date            Author          Comment
-- 1.00     2020-03-19      Chris Black     JIRA reference: IBOS-6   Initial version
-- 
-- -------------------------------------------------------------------------------
USE ibos_hr;

DROP TABLE IF EXISTS ibos_hr.work_referral;

-- Define variable names, formats, keys
CREATE TABLE ibos_hr.work_referral (
   work_referral_id                 INT            COMMENT 'referral ID'                       PRIMARY KEY AUTO_INCREMENT
   ,employee_id                     INT            COMMENT 'employee ID'                       NOT NULL, FOREIGN KEY(employee_id) REFERENCES employee_name(employee_id)
   ,referral_date                   DATETIME       COMMENT 'work referral date'
   ,referral_company                VARCHAR(100)   COMMENT 'work referral company'
   ,referral_start                  DATETIME       COMMENT 'work referral start date'
   ,referral_end                    DATETIME       COMMENT 'work referral end date'
   ,referral_value                  INT            COMMENT 'work referral value'
   ,referral_amount                 INT            COMMENT 'work referral amount'

  )
COMMENT 'Contains work referral information';

INSERT INTO ibos_hr.work_referral (
  employee_id                 
 ,referral_date       
 ,referral_company             
 ,referral_start              
 ,referral_end                
 ,referral_value              
 ,referral_amount 
)
VALUES(  
 -- employee_id   
 1
 -- ,referral_date
 , DATE '2020-03-03'
 -- ,referral_company  
 , 'Centrica'
 -- ,referral_start    
 , DATE '2020-03-03'
 -- ,referral_end      
 , DATE '2020-04-04'
 -- ,referral_value   
  , 300
 -- ,referral_amount 
  , 300
 );

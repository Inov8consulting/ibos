-- -------------------------------------------------------------------------------- 
-- 
-- ibos_hr_employee_contract_record.sql
-- 
-- Build database for employee record to integrate with iBOS CRM system 
-- 
-- Created Objects:
-- ibos_hr.employee_contract_record
--
-- Used Objects: 
-- ibos_hr.employee_record, ibos_hr.employee_address, ibos_hr.employee_contact, ibos_hr.employee_contract, ibos_hr.employee_leaver, ibos_hr.employee_salary, ibos_hr.employee_bonus, ibos_hr.employee_role, ibos_hr.employee_holidays
-- 
-- 
-- Ver      Date            Author          Comment
-- 1.00     2020-04-20     Chris Black     JIRA reference: IBOS-84 - Initial version
-- 
-- -------------------------------------------------------------------------------
USE ibos_hr;

DROP TABLE IF EXISTS ibos_hr.employee_contract_record;

-- Define variable names, formats, keys
CREATE TABLE ibos_hr.employee_contract_record (
  record_id                INT            COMMENT 'records id'     PRIMARY KEY AUTO_INCREMENT
  ,contract_id             INT            COMMENT 'contract ID'    NOT NULL, FOREIGN KEY(contract_id) REFERENCES ibos_hr.employee_contract(contract_id)                   
  ,employee_id             VARCHAR(10)    COMMENT 'Employee ID'                            
  ,contract_date           DATETIME       COMMENT 'Contract date'
  ,contract_issued         VARCHAR(10)    COMMENT 'Contract issued?'
  ,start_date              VARCHAR(10)    COMMENT 'Contract start date as text'
  ,start_dt                DATETIME       COMMENT 'Start date'
  ,continuous_start_date   VARCHAR(10)    COMMENT 'Continuous start date as text'
  ,cont_start_dt           DATETIME       COMMENT 'Continuos start date'
  ,notice_period           VARCHAR(20)    COMMENT 'Notice period'
  ,employee_name           VARCHAR(350)   COMMENT 'Employee name'
  ,dob                     DATE           COMMENT 'Date of birth'
  ,employee_address        VARCHAR(1000)  COMMENT 'Address'
  ,address_1               VARCHAR(200)   COMMENT 'First line of address'             
  ,address_2               VARCHAR(200)   COMMENT 'Second line of address'             
  ,address_3               VARCHAR(200)   COMMENT 'Third line of address'              
  ,address_4               VARCHAR(200)   COMMENT 'Fourth line of address'             
  ,address_5               VARCHAR(200)   COMMENT 'Fifth line of address'              
  ,postcode                VARCHAR(20)    COMMENT 'Postal code'     
  ,employee_mobile         VARCHAR(20)    COMMENT 'Work mobile'
  ,employee_email          VARCHAR(200)   COMMENT 'Work email'
  ,employee_salary         VARCHAR(10)    COMMENT 'Salary as text'
  ,employee_salary_num     INT            COMMENT 'Salary'
  ,job_role                VARCHAR(50)    COMMENT 'Job role'
  ,job_level               VARCHAR(50)    COMMENT 'Job level'  
  ,holidays                INT            COMMENT 'Holiday allowance'
  ,bonus_num               INT            COMMENT 'Bonus value'
  ,bonus                   VARCHAR(10)    COMMENT 'Bonus as text'
  ,leave_dt                DATETIME       COMMENT 'Leave date'
  ,reason_for_leaving      VARCHAR(1000)  COMMENT 'Reason for leaving'
 
 )
COMMENT 'Contains employee name, address, contact details, contract, salary, bonus and job role';

INSERT INTO ibos_hr.employee_contract_record
SELECT NULL AS record_id 
     ,ec.contract_id
     ,ec.employee_id
     ,ec.contract_date
     ,ec.contract_issued
     ,CAST(DATE (ec.start_date) AS CHAR) AS start_date
     ,ec.start_date AS start_dt
     ,CAST(DATE (ec.continuous_start_date) AS CHAR) AS continuous_start_date
     ,ec.continuous_start_date AS cont_start_dt
     ,ec.notice_period
     ,CONCAT_WS(' ', en.title, en.first_name, en.other_names, en.last_name) AS employee_name
     ,en.date_of_birth AS dob
     ,ea.employee_address
     ,ea.address_1
     ,ea.address_2
     ,ea.address_3
     ,ea.address_4
     ,ea.address_5
     ,ea.postcode
     ,et.mobile_tel_num AS employee_mobile
     ,et.personal_email_addr AS employee_email
     ,CAST(es.employee_salary AS CHAR) AS employee_salary
     ,es.employee_salary AS employee_salary_num
     ,ej.job_role
     ,ej.job_level
     ,eh.holidays
     ,eb.bonus AS bonus_num
     ,CAST(eb.bonus AS CHAR) AS bonus
     ,el.leave_date AS leave_dt
     ,el.reason_for_leaving
FROM (SELECT ec1.contract_id
	 ,ec1.employee_id
     ,ec1.contract_date
     ,ec1.contract_issued
     ,ec1.start_date
     ,ec1.continuous_start_date
     ,ec1.notice_period
FROM ibos_hr.employee_contract ec1
INNER JOIN 
(SELECT employee_id, MAX(contract_date) as latest_contract
FROM ibos_hr.employee_contract
GROUP BY employee_id) ec2
ON ec1.employee_id = ec2.employee_id AND coalesce(ec1.contract_date,0) = coalesce(ec2.latest_contract,0)) AS ec
LEFT JOIN ibos_hr.employee_name en ON (ec.employee_id = en.employee_id)
LEFT JOIN (
     SELECT b.employee_id
          ,a.address_1
          ,a.address_2
          ,a.address_3
          ,a.address_4
          ,a.address_5
          ,a.postcode
          ,CONCAT_WS(', ', a.address_1, a.address_2, a.address_3, a.address_4, a.address_5, a.postcode) AS employee_address
     FROM ibos_hr.employee_address a
     INNER JOIN ibos_hr.employee_name_address b ON a.address_id = b.address_id
     WHERE a.address_type = 'Home'
          AND b.address_end = DATE ('9999-12-31')
     ) ea ON (ec.employee_id = ea.employee_id)
LEFT JOIN (
     SELECT b.employee_id
          ,a.mobile_tel_num
          ,a.personal_email_addr
     FROM ibos_hr.employee_contact_details a
     INNER JOIN ibos_hr.employee_name_contact_details b ON a.contact_id = b.contact_id
     WHERE b.contact_end = DATE ('9999-12-31')
     ) et ON (ec.employee_id = et.employee_id)
LEFT JOIN (
     SELECT a.employee_id
          ,a.employee_salary
     FROM ibos_hr.employee_salary a
     INNER JOIN (
          SELECT employee_id
               ,MAX(salary_date) AS latest_date
          FROM ibos_hr.employee_salary
          GROUP BY employee_id
          ) b ON a.employee_id = b.employee_id
          AND a.salary_date = b.latest_date
     ) es ON (ec.employee_id = es.employee_id)
LEFT JOIN (
     SELECT er2.employee_id
          ,er1.job_role_id
          ,jr.job_role
          ,jr.job_level
     FROM ibos_hr.employee_role AS er1
     INNER JOIN (
          SELECT employee_id
               ,MAX(role_start) AS latest_dt
          FROM ibos_hr.employee_role
          GROUP BY employee_id
          ) AS er2 ON er1.employee_id = er2.employee_id
          AND er1.role_start = er2.latest_dt
     LEFT JOIN ibos_hr.job_roles AS jr ON er1.job_role_id = jr.job_role_id
     ) ej ON (ec.employee_id = ej.employee_id)
LEFT JOIN (
     SELECT employee_id
          ,(holiday_allowance + days_carried_from_prev) - days_taken AS holidays
     FROM ibos_hr.employee_holidays
     WHERE calendar_year = YEAR(CURDATE())
     ) eh ON (ec.employee_id = eh.employee_id)
INNER JOIN (
     SELECT b.employee_id
          ,b.employee_bonus
          ,b.bonus_date
          ,s.employee_salary
          ,s.salary_date
          ,b.employee_bonus * s.employee_salary AS bonus
     FROM ibos_hr.employee_bonus b
     LEFT JOIN ibos_hr.employee_salary s ON b.employee_id = s.employee_id
          AND YEAR(b.bonus_date) = YEAR(s.salary_date)
     WHERE YEAR(s.salary_date) = YEAR(CURDATE()) - 1
     ) eb ON (ec.employee_id = eb.employee_id)
LEFT JOIN ibos_hr.employee_leaver el ON (ec.employee_id = el.employee_id)
;


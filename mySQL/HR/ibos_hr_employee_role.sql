-- -------------------------------------------------------------------------------- 
-- 
-- ibos_hr_employee_role.sql
-- 
-- Build table for employee roles to integrate with iBOS CRM system 
-- 
-- Created Objects:
-- ibos_hr.employee_role
--
-- Used Objects: 
-- 
-- 
-- Ver      Date            Author          Comment
-- 1.00     2020-03-19      Chris Black     JIRA reference: IBOS-6   Initial version
-- 
-- -------------------------------------------------------------------------------
USE ibos_hr;

-- DROP TABLE IF EXISTS ibos_hr.employee_role;

-- Define variable names, formats, keys
CREATE TABLE ibos_hr.employee_role (
   role_id               INT            COMMENT 'role ID'          PRIMARY KEY AUTO_INCREMENT
  ,job_role_id           INT            COMMENT 'job role ID'      NOT NULL ,FOREIGN KEY(job_role_id) REFERENCES job_roles(job_role_id)
  ,employee_id           INT            COMMENT 'Employee ID'      NOT NULL ,FOREIGN KEY (employee_id) REFERENCES employee_name(employee_id)
  ,role_start            DATETIME       COMMENT 'role start date'  
  )
COMMENT 'Contains employee role information';

-- Insert sample data values into table
INSERT INTO ibos_hr.employee_role (
  job_role_id
  ,employee_id 
  ,role_start
) 

VALUES 
 (   
 -- job_role_id
1
 --   ,employee_id 
,1
--   ,role_start
,DATE '2020-03-02'

),
 (   
 -- job_role_id
1 
--   ,employee_id 
,2
--   ,role_start
,DATE '2019-01-20'

),
 ( 
  -- job_role_id
1
--   ,employee_id 
,3
--   ,role_start
,DATE '2020-01-01'
),

 (   
  -- job_role_id
1
--   ,employee_id 
,3
--   ,role_start
,DATE '2019-01-01'
)
;

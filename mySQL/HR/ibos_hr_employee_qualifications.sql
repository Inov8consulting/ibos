-- -------------------------------------------------------------------------------- 
-- 
-- ibos_hr_employee_qualifications.sql
-- 
-- Build table for employee qualifications to integrate with iBOS CRM system 
-- 
-- Created Objects:
-- ibos_hr.employee_qualifications, 
--
-- Used Objects: 
-- 
-- 
-- Ver      Date            Author          Comment
-- 1.00     2020-03-19      Chris Black     JIRA reference: IBOS-6   Initial version
--
-- -------------------------------------------------------------------------------
USE ibos_hr;

-- DROP TABLE IF EXISTS ibos_hr.employee_qualifications;

-- Define variable names, formats, keys
CREATE TABLE ibos_hr.employee_qualifications (
  qualifications_id              INT            COMMENT 'Primary key'                                PRIMARY KEY AUTO_INCREMENT 
  ,employee_id                   INT            COMMENT 'Employee user identifier'                   NOT NULL ,FOREIGN KEY (employee_id) REFERENCES employee_name(employee_id) 
  ,qualification_subject         VARCHAR(100)   COMMENT 'Subject studied'
  ,qualification_level           VARCHAR(50)    COMMENT 'Level'                                    
  ,qualification_attained        VARCHAR(50)    COMMENT 'Qualification Attained'             
  ,qualification_body            VARCHAR(100)   COMMENT 'Qualification awarding body'                	
  ,qualification_date            DATE           COMMENT 'Date qualification awarded'                     
)
COMMENT 'Contains information on employee qualifications';

-- DELETE FROM ibos_hr.employee_qualifications;

-- Insert sample data values into table
INSERT INTO ibos_hr.employee_qualifications (
  employee_id                    
  ,qualification_subject        
  ,qualification_level                                          
  ,qualification_attained          
  ,qualification_body                 	
  ,qualification_date             
)

VALUES 
 (
--    employee_id        
 1           
--   ,qualification_subject   
, 'SAS'    
--   ,qualification_level   
, 'Base'                                        
--   ,qualification_attained 
, 'Pass'          
--   ,qualification_body     
, 'SAS'             	
--   ,qualification_date    
, '2020-01-01'    
),

 (
--    employee_id        
 2           
--   ,qualification_subject   
, 'AWS'    
--   ,qualification_level   
, 'Solutions Architect'                                        
--   ,qualification_attained 
, 'Associate'          
--   ,qualification_body     
, 'AWS'             	
--   ,qualification_date    
, '2020-02-01'    
),

 (
--    employee_id        
 3           
--   ,qualification_subject   
, 'SAS'    
--   ,qualification_level   
, 'Base'                                        
--   ,qualification_attained 
, 'Pass'          
--   ,qualification_body     
, 'SAS'             	
--   ,qualification_date    
, '2020-03-01'    
)
;




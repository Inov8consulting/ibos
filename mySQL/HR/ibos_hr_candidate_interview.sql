-- -------------------------------------------------------------------------------- 
-- 
-- ibos_hr_candidate_interview.sql
-- 
-- Build table for candidate interviews to integrate with iBOS CRM system 
-- 
-- Created Objects:
-- ibos_hr.candidate_interview
--
-- Used Objects: 
-- 
-- 
-- Ver      Date            Author          Comment
-- 1.00     2020-03-19      Chris Black     JIRA reference: IBOS-6   Initial version
-- 
-- -------------------------------------------------------------------------------
USE ibos_hr;

-- DROP TABLE IF EXISTS ibos_hr.candidate_interview;

-- Define variable names, formats, keys
CREATE TABLE ibos_hr.candidate_interview (
    interview_id                    INT            COMMENT 'interview id'                    PRIMARY KEY AUTO_INCREMENT
   ,candidate_id                    INT            COMMENT 'candidate id'                    NOT NULL ,FOREIGN KEY (candidate_id) REFERENCES candidate_record(candidate_id)
   ,interview_date                  DATETIME       COMMENT 'interview date'                  
   ,interview_type                  VARCHAR(100)   COMMENT 'interview type'
   ,interview_number                INT            COMMENT 'interview number'
   ,interview_location              VARCHAR(100)   COMMENT 'location'
   ,interview_notes                 VARCHAR(1000)  COMMENT 'notes'
   ,interview_attachment            VARCHAR(200)   COMMENT 'OLE object'
   ,interview_outcome               VARCHAR(50)    COMMENT 'outcome'
   ,interviewer_1                   VARCHAR(100)   COMMENT '1st interviewer'
   ,interviewer_2                   VARCHAR(100)   COMMENT '2nd interviewer'
	)
COMMENT 'Contains candidate interview information';

-- insert values into table

INSERT INTO ibos_hr.candidate_interview (
    candidate_id                    
   ,interview_date             
   ,interview_type
   ,interview_number
   ,interview_location
   ,interview_notes
   ,interview_attachment
   ,interview_outcome
   ,interviewer_1
   ,interviewer_2
)

VALUES (
  -- candidate_id   
 1 
--    ,interview_date      
, DATE '2020-04-03'       
--    ,interview_type
, 'In person'
--    ,interview_number
, 1
--    ,interview_location
, 'Spaces - Glasgow'
--    ,interview_notes
, 'Blah'
--    ,interview_attachment
, 'sharepoint link'
--    ,interview_outcome
, 'Contract offered'
--    ,interviewer_1
, 'Calum Mather'
--    ,interviewer_2
, 'Dominic Townsend'
);
	
-- -------------------------------------------------------------------------------- 
-- 
-- ibos_hr_employee_referral.sql
-- 
-- Build table for employee referral to integrate with iBOS CRM system 
-- 
-- Created Objects:
-- ibos_hr.employee_referral
--
-- Used Objects: 
-- 
-- 
-- Ver      Date            Author          Comment
-- 1.00     2020-03-19      Chris Black     JIRA reference: IBOS-6   Initial version
-- 
-- -------------------------------------------------------------------------------
USE ibos_hr;

-- DROP TABLE IF EXISTS ibos_hr.employee_referral;

-- Define variable names, formats, keys
CREATE TABLE ibos_hr.employee_referral (
   referral_id                 INT            COMMENT 'referral ID'                       PRIMARY KEY AUTO_INCREMENT
   ,employee_id                INT            COMMENT 'Employee ID for referrer'          NOT NULL, FOREIGN KEY (employee_id) REFERENCES employee_name (employee_id)
   ,employee_id_referred       INT            COMMENT 'Employee ID for referred'          
   ,employee_referral_amount   INT            COMMENT 'Referral amount'
  )
COMMENT 'Contains employee referral information';

-- Insert sample data values into table
INSERT INTO ibos_hr.employee_referral (
  employee_id
  ,employee_id_referred  
  ,employee_referral_amount   
) 

VALUES 
 ( -- employee_id_referrer 
 1
--   ,employee_id_referred
, 2
--   ,employee_referral_amount 
, 200
  ),
( -- employee_id_referrer 
 3
--   ,employee_id_referred
, 1
--   ,employee_referral_amount 
, 200
  ),

( -- employee_id_referrer 
 2
--   ,employee_id_referred
, 3
--   ,employee_referral_amount 
, 200
  );


	
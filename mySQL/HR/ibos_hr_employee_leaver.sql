-- -------------------------------------------------------------------------------- 
-- 
-- ibos_hr_employee_leaver.sql
-- 
-- Build table for employee leaver to integrate with iBOS CRM system 
-- 
-- Created Objects:
-- ibos_hr.employee_leaver
--
-- Used Objects: 
-- 
-- 
-- Ver      Date            Author          Comment
-- 1.00     2020-03-19      Chris Black     JIRA reference: IBOS-6   Initial version
-- 
-- -------------------------------------------------------------------------------
USE ibos_hr;

-- DROP TABLE IF EXISTS ibos_hr.employee_leaver;

-- Define variable names, formats, keys
CREATE TABLE ibos_hr.employee_leaver (
  leaver_id             INT            COMMENT 'leaver ID'                                PRIMARY KEY AUTO_INCREMENT
  ,employee_id          INT            COMMENT 'Employee ID'                              NOT NULL ,FOREIGN KEY (employee_id) REFERENCES employee_name (employee_id)  
  ,leave_date           DATETIME       COMMENT 'leaver date'                              
  ,reason_for_leaving   VARCHAR(1000)  COMMENT 'reason for leaving'                       
  )
COMMENT 'Contains employee leaver information';


-- Insert sample data values into table
INSERT INTO ibos_hr.employee_leaver (
  employee_id 
  ,leave_date 
  ,reason_for_leaving     
) 

VALUES 
 (   
--   employee_id 
2
--   ,leave_date 
,DATE '2020-01-31'
--   ,reason_for_leaving     
,'New job'
)
;


-- -------------------------------------------------------------------------------- 
-- 
-- ibos_hr_employee_bonus.sql
-- 
-- Build table for employee bonus to integrate with iBOS CRM system 
-- 
-- Created Objects:
-- ibos_hr.employee_bonus, ibos_hr.employee_bonus_last
--
-- Used Objects: 
-- 
-- 
-- Ver      Date            Author          Comment
-- 1.00     2020-03-19      Chris Black     JIRA reference: IBOS-6    Initial version
-- 
-- -------------------------------------------------------------------------------
USE ibos_hr;

-- DROP TABLE IF EXISTS ibos_hr.employee_bonus;

-- Define variable names, formats, keys
CREATE TABLE ibos_hr.employee_bonus (
   bonus_id               INT            COMMENT 'bonus ID'          PRIMARY KEY AUTO_INCREMENT
  ,employee_id            INT            COMMENT 'Employee ID'       NOT NULL ,FOREIGN KEY(employee_id) REFERENCES employee_name(employee_id)
  ,employee_bonus         DECIMAL(5,3)   COMMENT 'bonus'             
  ,bonus_date             DATETIME       COMMENT 'bonus date'        
  )
COMMENT 'Contains employee bonus information';

-- Insert sample data values into table
INSERT INTO ibos_hr.employee_bonus (
 employee_id             
 ,employee_bonus         
 ,bonus_date           
) 

VALUES 
 (   
--  employee_id 
1        
--   ,employee_bonus
, 0        
--   ,bonus_date 
, DATE '2020-03-02' 
), 

 (   
--  employee_id 
2             
--   ,employee_bonus
, 0.1         
--   ,bonus_date 
, DATE '2020-01-01' 
), 

 (   
--  employee_id 
3            
--   ,employee_bonus
, 0.08        
--   ,bonus_date 
, DATE '2019-01-01' 
), 

 (   
--  employee_id 
3        
--   ,employee_bonus
, 0.1       
--   ,bonus_date 
, DATE '2020-01-01' 
)
;


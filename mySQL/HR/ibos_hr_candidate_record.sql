-- -------------------------------------------------------------------------------- 
-- 
-- ibos_hr_candidate_record.sql
-- 
-- Build table for candidates to integrate with iBOS CRM system 
-- 
-- Created Objects:
-- ibos_hr.candidate_record
--
-- Used Objects: 
-- 
-- 
-- Ver      Date            Author          Comment
-- 1.00     2020-03-19      Chris Black     JIRA reference: JIRA reference: IBOS-6    Initial version
--
-- -------------------------------------------------------------------------------
USE ibos_hr;

-- DROP TABLE IF EXISTS ibos_hr.candidate_record;

-- Define variable names, formats, keys
CREATE TABLE ibos_hr.candidate_record (
    candidate_id                    INT            COMMENT 'candidate id'                    PRIMARY KEY AUTO_INCREMENT
   ,candidate_name                  VARCHAR(200)   COMMENT 'candidate name'                  
   ,candidate_role                  VARCHAR(50)    COMMENT 'candidate job role'
   ,candidate_source                VARCHAR(200)   COMMENT 'source'
   ,candidate_email                 VARCHAR(200)   COMMENT 'email'
   ,candidate_mobile                VARCHAR(20)    COMMENT 'mobile telephone number'
   ,candidate_phone                 VARCHAR(20)    COMMENT 'home telephone number'
   ,candidate_other_contact         VARCHAR(20)    COMMENT 'other contact number'
   ,candidate_skype                 VARCHAR(20)    COMMENT 'candidate skype details'
   ,candidate_date                  DATETIME       COMMENT 'date candidate was contacted'
   ,_status                         VARCHAR(100)   COMMENT 'status'
   ,current_employer                VARCHAR(200)   COMMENT 'candidates current employer'
   ,reason_for_leaving              VARCHAR(200)   COMMENT 'reason for leaving last jon'
   ,cv                              VARCHAR(1000)  COMMENT 'OLE object for CV document'
   ,cv_rating                       VARCHAR(10)    COMMENT 'CV rating'
   ,comments                        VARCHAR(2000)  COMMENT 'Comments on CV'
   ,notice_period                   VARCHAR(20)    COMMENT 'Length of notice required in current role'
   ,current_salary                  VARCHAR(10)    COMMENT 'Candidates current salary'
   ,requested_salary                VARCHAR(10)    COMMENT 'Candidates salary request'
   ,outcome                         VARCHAR(1000)  COMMENT 'Outcome'
   ,candidate_notified              VARCHAR(10)    COMMENT 'Candidate notified'
   ,notification_date               DATETIME       COMMENT 'Date candidate notified'
   ,notification_type               VARCHAR(20)    COMMENT 'Type of notification'
   ,new_employee_id                 INT            COMMENT 'Employee ID'
   ,created_date                    DATETIME       COMMENT 'Created date'
   ,created_by                      VARCHAR(100)   COMMENT 'Created by'
   ,updated_date                    DATETIME       COMMENT 'Update date'
   ,updated_by                      VARCHAR(100)   COMMENT 'Updated by'
  )
COMMENT 'Contains candidate information';

-- insert values into table

INSERT INTO ibos_hr.candidate_record (
    candidate_id
   ,candidate_name
   ,candidate_role
   ,candidate_source
   ,candidate_email
   ,candidate_mobile
   ,candidate_phone
   ,candidate_other_contact
   ,candidate_skype
   ,candidate_date
   ,_status
   ,current_employer
   ,reason_for_leaving
   ,cv
   ,cv_rating
   ,comments
   ,notice_period
   ,current_salary
   ,requested_salary
   ,outcome
   ,candidate_notified
   ,notification_date
   ,notification_type
   ,new_employee_id
   ,created_date
   ,created_by
   ,updated_date
   ,updated_by
)

VALUES (
-- candidate_id
NULL 
--    ,candidate_name
,'Joe Bloggs'
--    ,candidate_role
, 'Data Analyst'
--    ,candidate_source
, 'LinkedIn'
--    ,candidate_email
, 'JoeBlogs@hotmail.co.uk'
--    ,candidate_mobile
, '07712345678'
--    ,candidate_phone
, '01411234567'
--    ,candidate_other_contact
, ''
--    ,candidate_skype
, ''
--    ,candidate_date
, DATE '2020-03-30'
--    ,_status
, 'In progress'
--    ,current_employer
, 'HSBC'
--    ,reason_for_leaving
, 'Redundancy'
--    ,cv
, ''
--    ,cv_rating
, 'Good'
--    ,comments
, 'Blah blah blah'
--    ,notice_period
, '6 weeks'
--    ,current_salary
, '40000'
--    ,requested_salary
, '45000'
--    ,outcome
, 'Offered contract'
--    ,candidate_notified
, 'Y'
--    ,notification_date
, '2020-04-01'
--    ,notification_type
, 'email'
--    ,new_employee_id
, 4
--    ,created_date
, DATE '2020-03-01'
--    ,created_by
, ''
--    ,updated_date
, DATE '2020-04-01'
--    ,updated_by
, 'Mather, Calum'
)
;

	
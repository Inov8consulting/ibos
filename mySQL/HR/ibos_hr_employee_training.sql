-- -------------------------------------------------------------------------------- 
-- 
-- ibos_hr_employee_training.sql
-- 
-- Build table for employee training to integrate with iBOS CRM system 
-- 
-- Created Objects:
-- ibos_hr.employee_training
--
-- Used Objects: 
-- 
-- 
-- Ver      Date            Author          Comment
-- 1.00     2020-03-19      Chris Black     JIRA reference: IBOS-6   Initial version
-- 
-- -------------------------------------------------------------------------------
USE ibos_hr;

-- DROP TABLE IF EXISTS ibos_hr.employee_training;

-- Define variable names, formats, keys
CREATE TABLE ibos_hr.employee_training (
   training_id               INT             COMMENT 'training ID'               PRIMARY KEY AUTO_INCREMENT
   ,employee_id              INT             COMMENT 'Employee ID'               NOT NULL ,FOREIGN KEY (employee_id) REFERENCES employee_name(employee_id)
   ,course_name              VARCHAR(100)    COMMENT 'Course name'   
   ,course_provider          VARCHAR(100)    COMMENT 'Course provider'   
   ,course_ref               VARCHAR(10)     COMMENT 'course reference'   
   ,course_cost              INT             COMMENT 'Course cost'   
   ,course_participants      VARCHAR(100)    COMMENT 'Cost participants'   
   ,course_completion        DATETIME        COMMENT 'Course completed date'   
  )
COMMENT 'Contains employee training information';

INSERT INTO ibos_hr.employee_training(
   employee_id           
   ,course_name            
   ,course_provider   
   ,course_ref      
   ,course_cost       
   ,course_participants    
   ,course_completion       
)

VALUES
(
 --   employee_id   
 1
--    ,course_name   
, 'SAS'
--    ,course_provider  
, 'SAS'
--    ,course_ref      
, 'SAS101'
--    ,course_cost      
 , 300
--    ,course_participants   
 , ''
--    ,course_completion
, DATE('2020-04-02')
),
(
 --   employee_id   
 1
--    ,course_name   
, 'Python'         
--    ,course_provider  
, 'Udemy'
--    ,course_ref 
, '1234'     
--    ,course_cost      
, 20
 --    ,course_participants   
, '1'
 --    ,course_completion
, DATE('2020-03-29')
),

(
 --   employee_id   
 2
--    ,course_name   
,'AWS'        
--    ,course_provider  
,'AWS' 
--    ,course_ref      
,'AWS123'
--    ,course_cost      
,200 
--    ,course_participants   
,''
--    ,course_completion
, DATE('2020-02-02')
);



-- -------------------------------------------------------------------------------- 
-- 
-- ibos_hr_employee_holidays.sql
-- 
-- Build database for employee holidays to integrate with iBOS CRM system 
-- 
-- Created Objects:
-- ibos_hr.employee_holidays
--
-- Used Objects: 
-- 
-- 
-- Ver      Date            Author          Comment
-- 1.00     2020-03-19      Chris Black     JIRA reference: IBOS-6   Initial version
-- 
-- -------------------------------------------------------------------------------
USE ibos_hr;

-- DROP TABLE IF EXISTS ibos_hr.employee_holidays;

-- Define variable names, formats, keys
CREATE TABLE ibos_hr.employee_holidays (
   holiday_id               INT            COMMENT 'holidays ID'                        PRIMARY KEY AUTO_INCREMENT
  ,employee_id              INT            COMMENT 'Employee ID'                        NOT NULL, FOREIGN KEY (employee_id) REFERENCES employee_name(employee_id) 
  ,calendar_year            INT            COMMENT 'calendar year'                      NOT NULL     
  ,holiday_allowance        INT            COMMENT 'holiday allowance'                    
  ,days_taken               INT            COMMENT 'days taken'
  ,days_carried_from_prev   INT            COMMENT 'days carried forward from last year'
  )
COMMENT 'Contains employee holidays information';

-- Insert sample data values into table
INSERT INTO ibos_hr.employee_holidays (
  employee_id        
  ,calendar_year         
  ,holiday_allowance                    
  ,days_taken           
  ,days_carried_from_prev           
) 

VALUES 
 (   
 -- employee_id        
 1
--   ,calendar_year 
, 2020       
--   ,holiday_allowance  
,28                  
--   ,days_taken        
, 1  
--   ,days_carried_from_prev  
, 0
), 
(
 -- employee_id        
 2
--   ,calendar_year 
,2020       
--   ,holiday_allowance  
,28                  
--   ,days_taken        
, 8 
--   ,days_carried_from_prev  
, 6
),
(
 -- employee_id        
 3
--   ,calendar_year 
,2020       
--   ,holiday_allowance  
,28                  
--   ,days_taken        
, 5
--   ,days_carried_from_prev  
, 1
),
(
 -- employee_id        
 3
--   ,calendar_year 
,2019       
--   ,holiday_allowance  
,28                  
--   ,days_taken        
, 27
--   ,days_carried_from_prev  
, 5
)
;
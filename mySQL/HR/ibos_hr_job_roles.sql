-- -------------------------------------------------------------------------------- 
-- 
-- ibos_hr_job_roles.sql
-- 
-- Build database for job roles to integrate with iBOS CRM system 
-- 
-- Created Objects:
-- ibos_hr.job_roles
--
-- Used Objects: 
-- 
-- 
-- Ver      Date            Author          Comment
-- 1.00     2020-03-19      Chris Black     JIRA reference: IBOS-6   Initial version
-- 
-- -------------------------------------------------------------------------------
USE ibos_hr;

-- DROP TABLE IF EXISTS ibos_hr.job_roles;

-- Define variable names, formats, keys
CREATE TABLE ibos_hr.job_roles (
   job_role_id           INT            COMMENT 'role ID'          PRIMARY KEY AUTO_INCREMENT
  ,job_role              VARCHAR(50)    COMMENT 'job role'         NOT NULL 
  ,job_level             VARCHAR(50)    COMMENT 'job level'        NOT NULL
  )
COMMENT 'Contains job roles information';

-- Insert sample data values into table
INSERT INTO ibos_hr.job_roles (
  job_role
  ,job_level
) 

VALUES 
 (   

--   ,job_role
'B.I. CONSULTANT'
--   ,job_level
,'CONSULTANT'

),
 (   
 --   ,job_role
'B.I. CONSULTANT'
--   ,job_level
,'LEAD CONSULTANT'

),
 (   
--   ,job_role
'H.R. MANAGER'
--   ,job_level
,'MANAGER'

),

 (   
--   ,job_role
'BUSINESS DEVELOPMENT MANAGER'
--   ,job_level
,'MANAGER'
)
;




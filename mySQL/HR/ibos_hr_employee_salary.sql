-- -------------------------------------------------------------------------------- 
-- 
-- ibos_hr_employee_salary.sql
-- 
-- Build table for employee salary to integrate with iBOS CRM system 
-- 
-- Created Objects:
-- ibos_hr.employee_salary
--
-- Used Objects: 
-- 
-- 
-- Ver      Date            Author          Comment
-- 1.00     2020-03-19      Chris Black     JIRA reference: IBOS-6   Initial version
-- 
-- -------------------------------------------------------------------------------
USE ibos_hr;

-- DROP TABLE IF EXISTS ibos_hr.employee_salary;

-- Define variable names, formats, keys
CREATE TABLE ibos_hr.employee_salary (
   salary_id               INT            COMMENT 'salary ID'          PRIMARY KEY AUTO_INCREMENT
  ,employee_id             INT            COMMENT 'Employee ID'        NOT NULL, FOREIGN KEY(employee_id) REFERENCES employee_name(employee_id)
  ,employee_salary         INT            COMMENT 'Salary'             
  ,salary_date             DATETIME       COMMENT 'Salary date'        
  )
COMMENT 'Contains employee salary information';

-- Insert sample data values into table
INSERT INTO ibos_hr.employee_salary (
 employee_id             
 ,employee_salary         
 ,salary_date           
) 

VALUES 
 (   
--  employee_id 
1       
--   ,employee_salary
, 100000         
--   ,salary_date 
, DATE '2020-03-02' 
), 

 (   
--  employee_id 
2       
--   ,employee_salary
, 150000         
--   ,salary_date 
, DATE '2020-01-01' 
), 

 (   
--  employee_id 
3        
--   ,employee_salary
, 100000         
--   ,salary_date 
, DATE '2018-10-01' 
), 

 (   
--  employee_id 
3      
--   ,employee_salary
, 1000000         
--   ,salary_date 
, DATE '2020-01-01' 
)
;
